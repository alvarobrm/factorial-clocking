# factorial-clocking

This project allow clock in Factorial automatically using Selenium test and Google Sing in.

###Requirements
- Linux / Mac
- Maven 
- Chrome 90


##Usage

This project uses the Google account to singin. To be able to do the singin its necessary set two environment variables
in Linux/Mac terminal run: 

```
export FACTORIAL_USERNAME={{your google account}} 
export FACTORIAL_ENCRYPTED_PASSWORD={{your Base64 password}}
export ENV={{your SO (Linux or Mac)}}
```

After that, simply run:

```
mvn verify
```

#CLI usage

The project include fclock CLI in order to make easier the usage. It's necesary create the file fclock.properties inside 
bin folder. This file has to contain this properties:

```
factorial.user={{your google account}}
factorial.password={{your Base64 password}}
```

##CLI install

To install the CLI is need export the PATH of the project, here is explained how to do usin zsh bash:

- open the file `~/.zshrc` and add:

```
FACTORIAL_CLOCKIN_HOME=/path/to/project/factorial-clocking/src/main/
export PATH=$FACTORIAL_CLOCKIN_HOME/bin:$PATH
```


