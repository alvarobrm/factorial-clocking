import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class FactorialDashboardPage extends BasePage {


    private final By clockInStartButton = By.xpath("//*[text()='Entrada']");
    private final By clockInEndButton = By.xpath("//*[text()='Detener']");
    //private final By clockInConfirmButton = By.cssSelector("body > div:nth-child(19) > div > div > div > form > div > button:nth-child(1) > div > div");
    private final By clockInConfirmButton = By.xpath("//*[contains(text(),'Fichar salida ')]");

    public FactorialDashboardPage(WebDriver driver) {
        super(driver);
    }

    private WebElement getClockStartInButton(){
        final var wait = new WebDriverWait(getDriver(), 30);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(clockInStartButton));
    }

    private WebElement getClockEndInButton(){
        final var wait = new WebDriverWait(getDriver(), 30);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(clockInEndButton));
    }

    private WebElement getClockEndInConfirmButton(){
        final var wait = new WebDriverWait(getDriver(), 30);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(clockInConfirmButton));
    }

    public FactorialDashboardPage clockInClick(){
        getClockStartInButton().click();
        return this;
    }

    public FactorialDashboardPage clockInEndClick(){
        getClockEndInButton().click();
        return this;
    }

    public FactorialDashboardPage clockInEndConfirmClick(){
        getClockEndInConfirmButton().click();
        return this;
    }
}
