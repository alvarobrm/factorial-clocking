import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

class FactorialClockInTest {

    private static WebDriver driver;
    private FactorialHomePage factorialHomePage;
    private FactorialDashboardPage factorialDashboardPage;

    @BeforeAll
    static void setUp() {
        final var env = System.getProperty("os.name");

        String driverPath;
        if(env.contains("Mac")) {
            driverPath = "./driver/mac/chromedriver";
            System.out.println("------- DRIVER : " + driverPath);
            System.setProperty("webdriver.chrome.driver", driverPath);
        } /*else {
            driverPath = "./driver/linux/chromedriver";
        }*/


        final ChromeOptions options = new ChromeOptions();
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("browserstack.timezone", "Madrid");
        //options.setCapability("browserstack.timezone","Madrid");
        options.merge(caps);
        options.addArguments("--window-size=1500,980");
        options.addArguments("--disable-extensions");
        options.addArguments("--disable-setuid-sandbox");
        options.addArguments("--proxy-server='direct://'");
        options.addArguments("--proxy-bypass-list=*");
        options.addArguments(("--lang=es_es"));
        options.addArguments("--disable-gpu");
        options.addArguments("--disable-web-security");
        //options.addArguments("--remote-debugin-port=9222");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--no-sandbox");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.17 Safari/537.36");
        options.setHeadless(true);
        driver = new ChromeDriver(options);
    }

    @BeforeEach
    void init() {
        factorialHomePage = new FactorialHomePage(driver);
        factorialDashboardPage = new FactorialDashboardPage(driver);
    }

    @AfterEach
    void end() {
        driver.quit();
    }

    @Test
    void shouldLogIn() throws InterruptedException {

        final var userName = System.getenv("FACTORIAL_USERNAME");
        final var password = System.getenv("FACTORIAL_ENCRYPTED_PASSWORD");
        factorialHomePage.insertUser(userName).insertPass(password).clickLogin();
        //Thread.sleep(5000);
        factorialDashboardPage.clockInClick();
    }

    @Test
    void shouldClockOut() throws InterruptedException {

        final var userName = System.getenv("FACTORIAL_USERNAME");
        final var password = System.getenv("FACTORIAL_ENCRYPTED_PASSWORD");
        factorialHomePage.insertUser(userName).insertPass(password).clickLogin();
        factorialDashboardPage.clockInEndClick();
        factorialDashboardPage.clockInEndConfirmClick();
    }
}
