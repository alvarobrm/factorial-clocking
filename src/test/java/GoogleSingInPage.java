import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Base64;

public class GoogleSingInPage extends BasePage {

    private final By userInput = By.cssSelector("#identifierId");
    private final By identifierNext = By.cssSelector("#identifierNext > div > button");

    //private final By passwordInput = By.cssSelector("#password > div > div > div > input");
    private final By passwordInput = By.cssSelector("#password > div.aCsJod.oJeWuf > div > div.Xb9hP > input");
    private final By passwordNext = By.cssSelector("#passwordNext > div > button");
    private final By captcha = By.xpath("//iframe[starts-with(@name, 'a-') and starts-with(@src, 'https://www.google.com/recaptcha')]");

    public GoogleSingInPage(WebDriver driver) {
        super(driver);
    }

    private WebElement getUserInput(){
        final Wait<WebDriver> wait = new WebDriverWait(getDriver(), 10);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(userInput));
    }

    private WebElement getPasswordInput(){
        final Wait<WebDriver> wait = new WebDriverWait(getDriver(), 30);
        return wait.until(ExpectedConditions.elementToBeClickable(passwordInput));
    }

    private WebElement getIdentifierNext(){
        final Wait<WebDriver> wait = new WebDriverWait(getDriver(), 10);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(identifierNext));
    }

    private WebElement getPasswordNext(){
        final Wait<WebDriver> wait = new WebDriverWait(getDriver(), 10);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(passwordNext));
    }

    private WebElement getCaptcha(){
        try{
            final Wait<WebDriver> wait = new WebDriverWait(getDriver(), 10);
            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(captcha));
            System.out.println("WE HAVE CAPTCHA");
            final Wait<WebDriver> wait2 = new WebDriverWait(driver, 20);
            return wait2.until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.recaptcha-checkbox-checkmark")));
        } catch (NoSuchElementException | TimeoutException e){
            System.out.println("NO CAPTCHA");
            return null;
        }

    }

    public GoogleSingInPage insertUser(String user) throws InterruptedException {
        getUserInput().sendKeys(user);
        return this;
    }

    public GoogleSingInPage insertPassword(String pass){
        final byte[] bytes = Base64.getDecoder().decode(pass);
        String decodePass = new String(bytes);
        getPasswordInput().sendKeys(decodePass);
        return this;
    }

    public GoogleSingInPage clickIdentifierNext(){
        getIdentifierNext().click();
        return this;
    }

    public GoogleSingInPage clickPasswordNext(){
        getPasswordNext().click();
        return this;
    }

    public GoogleSingInPage clickCaptcha(){

        final var captcha = getCaptcha();
        if (captcha != null){
            captcha.click();
        }
        return this;
    }

}
