import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Base64;

public class FactorialHomePage extends BasePage {

    public static final String URL = "https://app.factorialhr.com/dashboard";

    private final By googleButton = By.cssSelector("body > div.js-page-layout.layout > div.form__wrapper > div > div > div > div.form__container > ul > li:nth-child(1) > a");
    private final By emailInput = By.cssSelector("#user_email");
    private final By passwordInput = By.cssSelector("#user_password");
    private final By loginButton = By.cssSelector("#new_user > input.js-submit.button.button--brand.button--full-width");

    public FactorialHomePage(WebDriver driver) {
        super(driver);
        visit(URL);
    }

    public WebElement getGoogleButton(){
        final Wait<WebDriver> wait = new WebDriverWait(getDriver(), 10);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(googleButton));
    }

    private WebElement getUserInput(){
        final Wait<WebDriver> wait = new WebDriverWait(getDriver(), 10);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(emailInput));
    }

    private WebElement getLoginButton(){
        final Wait<WebDriver> wait = new WebDriverWait(getDriver(), 10);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(loginButton));
    }

    private WebElement getPasswordInput(){
        final Wait<WebDriver> wait = new WebDriverWait(getDriver(), 10);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(passwordInput));
    }

    public FactorialHomePage insertUser(String user) throws InterruptedException {
        getUserInput().sendKeys(user);
        return this;
    }

    public FactorialHomePage insertPass(String pass) throws InterruptedException {
        final byte[] bytes = Base64.getDecoder().decode(pass);
        String decodePass = new String(bytes);
        getPasswordInput().sendKeys(decodePass);
        return this;
    }

    public FactorialHomePage clickLogin(){
        getLoginButton().click();
        return this;
    }



}
